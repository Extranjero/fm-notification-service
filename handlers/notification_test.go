package handlers

import (
	"notification/models"

	"fm-libs/api"
	"fmt"
	"testing"
)

var (
	NotificationId string
)

func createWatchDeps() error {
	model := models.Notification{}
	model.UserId = UID
	model.Content = `{"field": "value"}`
	model.FleetId = FID
	model.CreatedBy = UID

	if err := model.Create(); err != nil {
		return fmt.Errorf("notification create error: %s", err)
	}

	NotificationId = model.Id

	return nil
}

func TestNotifGetAll(t *testing.T) {
	if err := createWatchDeps(); err != nil {
		t.Error("deps create error: ", err)
	}

	asserts := []Assert{
		{NotificationJson{BaseRpcReq: s}, api.ErrCodeSuccess},
	}

	_, err := Expect("Notification.GetAll", asserts)
	if err != nil {
		t.Error(err)
		return
	}
}

func TestNotifSetView(t *testing.T) {
	asserts := []Assert{
		{NotificationJson{BaseRpcReq: s, NtfIds: []string{NotificationId}}, api.ErrCodeSuccess},
	}

	_, err := Expect("Notification.SetView", asserts)
	if err != nil {
		t.Error(err)
		return
	}

}
