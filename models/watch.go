package models

import (
	"fm-libs/dbutil"
	"fm-libs/orm"
	"fm-libs/util"

	"fmt"
)

const (
	ActionCreate = 1 << iota
	ActionUpdate
	ActionDelete
	ActionRestore
)

var (
	mapStrInt_actions = make(map[string]int)
)

func init() {
	mapStrInt_actions["created"] = ActionCreate
	mapStrInt_actions["updated"] = ActionUpdate
	mapStrInt_actions["deleted"] = ActionDelete
	mapStrInt_actions["restored"] = ActionRestore
}

type Watch struct {
	Id      string `db:"id" json:"id"`
	Entity  string `db:"entity" json:"entity"`
	Actions int    `db:"actions" json:"actions"`
	UserId  string `db:"user_id" json:"user_id"`

	orm.Std
}

func (m Watch) GetName() string {
	return "watch"
}

func (m Watch) GetTable() string {
	return "ntf_watches"
}

func (m *Watch) SetId(id string) {
	m.Id = id
}

//Orm helpers. Making models acting like active record
func (m *Watch) Create(schema string) error {
	args := orm.Args{"schema": schema, "table": m.GetTable()}
	return ORM.Insert(m, args, true)
}

func (m *Watch) Update(schema string) error {
	args := orm.Args{"schema": schema, "table": m.GetTable()}
	return ORM.Update(m, args)
}

func (m *Watch) Get(schema, id string) error {
	return ORM.Get(m, orm.ModelSqlFile(m, "getbypk"), orm.Args{
		"schema": schema,
		"table":  m.GetTable(),
	}, id)
}

func (m *Watch) GetAll(schema string, sort []string, page, pageSize int) ([]Watch, int, error) {
	dests := []Watch{}

	order := dbutil.OrderMap(map[string]string{
		"id": "id",
	}, sort, "id ASC")

	cnt, err := m.Count(schema)
	if err != nil {
		return nil, 0, err
	}

	offset, pages := util.GetPageOffset(page, pageSize, cnt)

	if offset == -1 {
		return nil, 0, ErrNotFound
	}

	if pages == -1 {
		return nil, 0, ErrPageNotFound
	}

	if pageSize < 1 {
		pageSize = 10
	}

	err = ORM.Select(&dests, orm.ModelSqlFile(m, "select"), orm.Args{
		"schema": schema,
		"table":  m.GetTable(),
	}, order, pageSize, offset)

	if err != nil {
		return nil, 0, err
	}

	return dests, pages, nil
}

func (m *Watch) Delete(schema string) error {
	args := orm.Args{"schema": schema, "table": m.GetTable()}
	return ORM.Delete(m, args)
}

func (m *Watch) Restore(schema string) error {
	args := orm.Args{"schema": schema, "table": m.GetTable()}
	return ORM.Restore(m, args)
}

func (m *Watch) Count(schema string) (int, error) {
	var count int
	args := orm.Args{
		"schema": schema,
		"table":  m.GetTable(),
	}
	err := ORM.Get(&count, orm.ModelSqlFile(m, "count"), args)

	if err != nil {
		return 0, err
	}

	return count, nil
}

func (m *Watch) ConvertActions(actions []string) (int, error) {
	var action_bits int
	for _, action := range actions {
		action_bit, ok := mapStrInt_actions[action]
		if !ok {
			return action_bits, fmt.Errorf("action not found: %s", action)
		}

		action_bits += action_bit
	}

	return action_bits, nil
}

func (m *Watch) GetByMask(schema, entity, action string) ([]Watch, error) {
	var dests []Watch
	action_int, ok := mapStrInt_actions[action]
	if !ok {
		return dests, fmt.Errorf("action not found: %s", action)
	}

	err := ORM.Select(&dests, orm.ModelSqlFile(m, "select_by_mask"), orm.Args{
		"schema": schema,
		"table":  m.GetTable(),
	}, entity, action_int)

	return dests, err
}
