CREATE TABLE IF NOT EXISTS fleet_1d75255a_7362_4e3a_8c8d_7fdfe3eb687e.vehicle_vehicles(
        PRIMARY KEY (id)
) INHERITS (public.vehicle_vehicles);

INSERT INTO fleet_1d75255a_7362_4e3a_8c8d_7fdfe3eb687e.vehicle_vehicles
	(
		id, 
		name, 
		year,
		fuel_type,
		reg_state_id, 
		fuel_tank_size,
		color_id,
		type_id,
		man_id,
		model_id,
		created_by, 
		created_at,
		deleted
	) 
VALUES 
	(
		'1fc0b2d0-88cb-405b-afe2-ee04e574079c',
		'fuel_vehicle_name', 
		1994,
		1,
		'6aec3b6b-0f7c-4146-bbfc-729661c61f32', 
		100.0,
		'6aec3b6b-0f7c-4146-bbfc-729661c61f32',
		'6aec3b6b-0f7c-4146-bbfc-729661c61f32',
		'6aec3b6b-0f7c-4146-bbfc-729661c61f32',
		'6aec3b6b-0f7c-4146-bbfc-729661c61f32',
		'6aec3b6b-0f7c-4146-bbfc-729661c61f32', 
		now(),
		FALSE	
	);
