CREATE TABLE IF NOT EXISTS fleet_afbadde1_e698_4bea_a328_4819f4a44e45.vehicle_vehicles(
        PRIMARY KEY (id)
) INHERITS (public.vehicle_vehicles);

INSERT INTO fleet_afbadde1_e698_4bea_a328_4819f4a44e45.vehicle_vehicles
	(
		id, 
		name, 
		year,
		fuel_type,
		reg_state_id, 
		fuel_tank_size,
		color_id,
		type_id,
		man_id,
		model_id,
		created_by, 
		created_at,
		deleted
	) 
VALUES 
	(
		'aece4ba1-cc98-43e7-9773-ab462d6e2953',
		'fuel_vehicle_name', 
		1994,
		1,
		'754bbb75-b31d-4c2b-b1df-035f248d9e8e', 
		100.0,
		'754bbb75-b31d-4c2b-b1df-035f248d9e8e',
		'754bbb75-b31d-4c2b-b1df-035f248d9e8e',
		'754bbb75-b31d-4c2b-b1df-035f248d9e8e',
		'754bbb75-b31d-4c2b-b1df-035f248d9e8e',
		'754bbb75-b31d-4c2b-b1df-035f248d9e8e', 
		now(),
		FALSE	
	);
