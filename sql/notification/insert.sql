INSERT INTO {table}
(
  id,
  content,
  user_id,
  fleet_id,
  created_at,
  created_by
)
VALUES
(
  :id,
  :content,
  :user_id,
  :fleet_id,
  current_timestamp at time zone 'utc',
  :created_by
) RETURNING *
